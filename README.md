# Docker in Docker

## Config buildx
```
docker buildx create --use
```

## Build
```
docker buildx build --platform "linux/amd64,linux/arm64,linux/arm/v7,linux/arm/v6" --pull --push -t bitstrum/dind .
```

## Run
```
docker run --rm --privileged -it bitstrum/dind bash
```
