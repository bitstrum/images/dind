#!/bin/bash
set -e

# No error message, always OK: 2>/dev/null || true
for i in $(ls ${ENTRYPOINT_DIR}/*.sh ); do
  FILE="${i}"
  echo "Running ${FILE}"
  source ${FILE};
done

exec "$@"
