#!/bin/sh
set -e

BUILDX_VERSION="v0.4.1"

PLAT=$(uname -s)
case ${PLAT} in
  "Darwin")
    PLAT="darwin"
    ;;
  "Linux")
    PLAT="linux"
    ;;
  *)
    echo "Unsupported platform: ${PLAT}"
    exit 1
esac

ARCH=$(uname -m)
case ${ARCH} in
  "x86_64")
    ARCH="amd64"
    ;;
  "aarch64_be"|"aarch64"|"armv8b"|"armv8l")
    ARCH="arm64"
    ;;
  "armv7l")
    ARCH="arm-v7"
    ;;
  *)
    echo "Unsupported architecture: ${ARCH}"
    exit 1
    ;;
esac

mkdir -p ~/.docker/cli-plugins

URL="https://github.com/docker/buildx/releases/download/${BUILDX_VERSION}/buildx-${BUILDX_VERSION}.${PLAT}-${ARCH}"
echo "Downloading ${URL}"
curl -k -sSLf "${URL}" -o ~/.docker/cli-plugins/docker-buildx
chmod +x ~/.docker/cli-plugins/docker-buildx
