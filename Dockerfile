ARG CONTAINER_PREFIX
FROM ${CONTAINER_PREFIX}/debian:10

ARG DOCKER_VERSION=""
ENV ENTRYPOINT_DIR="/opt/entrypoint.d"

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN apt-get update && apt-get -y upgrade \
 && apt-get install --no-install-recommends --yes ca-certificates curl \
 && curl -sSLf https://get.docker.com | VERSION="${DOCKER_VERSION}" sh \
 && rm -rf /var/lib/apt/lists/* \
 && chmod +x /usr/local/bin/entrypoint.sh \
 && mkdir -p ${ENTRYPOINT_DIR}

COPY buildx-install.sh /opt/
RUN chmod +x /opt/buildx-install.sh \
 && /opt/buildx-install.sh

COPY docker-entrypoint.sh ${ENTRYPOINT_DIR}/docker-entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
